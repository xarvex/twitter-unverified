# Changelog

## [0.0.2] ([GitHub](https://github.com/xarvex/twitter-unverified/releases/tag/0.0.2)) - 2024-06-14

### Fixed

- `twitter.com` and `x.com` are both being used


## [0.0.1] ([GitHub](https://github.com/xarvex/twitter-unverified/releases/tag/0.0.1)) - 2024-06-14

### Fixed

- Correct faulty math comparison ([`9a0a8546`](https://gitlab.com/xarvex/twitter-unverified/commit/9a0a8546))
- **Breaking:** `twitter.com` is now `x.com` ([`96b0fea1`](https://gitlab.com/xarvex/twitter-unverified/commit/96b0fea1))


## [0.0.0] ([GitHub](https://github.com/xarvex/twitter-unverified/releases/tag/0.0.0)) - 2024-02-23

_Initial release._


[0.0.2]: https://gitlab.com/xarvex/twitter-unverified/tags/0.0.2
[0.0.1]: https://gitlab.com/xarvex/twitter-unverified/tags/0.0.1
[0.0.0]: https://gitlab.com/xarvex/twitter-unverified/tags/0.0.0
